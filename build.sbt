ThisBuild / name         := "http-kafka"
ThisBuild / description  := "A simple demo how to implement some HTTP service with Kafka"

ThisBuild / scalaVersion := "2.12.9"

ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings"
)

addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3")
addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")

val Http4sVersion     = "0.20.10"
val CirceVersion      = "0.11.1"
val DisruptorVersion  = "3.4.2"
val Fs2KafkaVersion   = "0.19.9"
val Log4jVersion      = "2.12.1"
val PureconfigVersion = "0.11.1"
val ScalatestVersion  = "3.0.8"
val SwaggerUiVersion  = "3.23.5"
val TapirVersion      = "0.9.3"

lazy val httpserver = (project in file("."))
  .enablePlugins(
    JavaAppPackaging,
    DockerPlugin)
  .settings(
    mainClass := Some("name.bastkowski.httpkafka.Application"),
    libraryDependencies ++= Seq(
      "com.github.pureconfig"    %% "pureconfig"                % PureconfigVersion,
      "com.softwaremill.tapir"   %% "tapir-core"                % TapirVersion,
      "com.softwaremill.tapir"   %% "tapir-http4s-server"       % TapirVersion,
      "com.softwaremill.tapir"   %% "tapir-openapi-circe-yaml"  % TapirVersion,
      "com.softwaremill.tapir"   %% "tapir-openapi-docs"        % TapirVersion,
      "com.lmax"                  % "disruptor"                 % DisruptorVersion,
      "com.ovoenergy"            %% "fs2-kafka"                 % Fs2KafkaVersion,
      "io.circe"                 %% "circe-generic"             % CirceVersion,
      "org.apache.logging.log4j"  % "log4j-api"                 % Log4jVersion,
      "org.apache.logging.log4j" %% "log4j-api-scala"           % "11.0",
      "org.apache.logging.log4j"  % "log4j-slf4j-impl"          % Log4jVersion,
      "org.http4s"               %% "http4s-blaze-client"       % Http4sVersion,
      "org.http4s"               %% "http4s-blaze-server"       % Http4sVersion,
      "org.http4s"               %% "http4s-circe"              % Http4sVersion,
      "org.http4s"               %% "http4s-dsl"                % Http4sVersion,
      "org.scalatest"            %% "scalatest"                 % ScalatestVersion    % "test",
      "org.webjars"               % "swagger-ui"                % SwaggerUiVersion),
    dockerBaseImage := "openjdk:12",
    dockerExposedPorts := Seq(8080),
    dockerUpdateLatest := true
  )
