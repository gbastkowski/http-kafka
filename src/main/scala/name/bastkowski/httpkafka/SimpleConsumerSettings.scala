package name.bastkowski.httpkafka

import fs2.kafka.{AutoOffsetReset, ConsumerSettings, Deserializer}

trait SimpleConsumerSettings {

  protected def bootstrapServers: String

  private[this] implicit val nameDeserializer: Deserializer[Name] =
    Deserializer.lift[Name] { bytes => Name(bytes.toString) }

  protected val consumerSettings: ConsumerSettings[String, Name] =
    ConsumerSettings[String, Name]
      .withAutoOffsetReset(AutoOffsetReset.Earliest)
      .withBootstrapServers(bootstrapServers)
      .withGroupId("group")
      .withAutoOffsetReset(AutoOffsetReset.Latest)
      .withEnableAutoCommit(true)

}
