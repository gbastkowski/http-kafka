package name.bastkowski.httpkafka

import cats.effect.{IO, _}
import cats.implicits._
import fs2.kafka._
import org.apache.logging.log4j.scala.Logging
import org.http4s.HttpRoutes
import tapir._
import tapir.server.http4s._

import scala.concurrent.ExecutionContext.Implicits.global

object HelloEndpoint extends Logging {

  private[this] implicit val cs: ContextShift[IO] = IO.contextShift(global)

  implicit val nameCodec: Codec[Name, MediaType.TextPlain, String] =
    Codec.stringPlainCodecUtf8.map(Name.apply)(_.value)

  val definition: Endpoint[Name, String, String, Nothing] = endpoint
    .get
    .in("hello" / path[Name]("name"))
    .errorOut(stringBody)
    .out(stringBody)

  def apply(producer: KafkaProducer[IO, String, Name]): HttpRoutes[IO] =
    definition.toRoutes { name =>
      producer
        .produce(message(name)).flatten
        .flatMap(n => IO{
                   logger.info("Sent Hi")
                   "Hi, " + n
                 })
        .map(_.asRight[String])
    }

  private def message(name: Name) = ProducerMessage.one(ProducerRecord("topic", "hello", name))
}

case class Name(value: String)
