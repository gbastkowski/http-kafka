package name.bastkowski.httpkafka

import cats.effect.{IO, _}
import cats.implicits._
import fs2.Stream
import fs2.kafka._
import org.apache.logging.log4j.scala.Logging
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze._
import org.http4s.server.staticcontent.WebjarService.Config
import org.http4s.server.staticcontent.webjarService
import pureconfig.generic.auto._
import tapir.docs.openapi._
import tapir.openapi.circe.yaml._

import scala.concurrent.ExecutionContext.Implicits.global

object Application extends App with Logging {
  logger.warn("Starting with workaround, see https://jira.apache.org/jira/browse/LOG4J2-2375. Don't delete this log message")

  new IOApp with SimpleConsumerSettings {

    def bootstrapServers: String = pureconfig.loadConfig[KafkaConfig] match {
      case Right(config) => config.bootstrapServers
      case Left(e) => throw new IllegalArgumentException(e.toString)
    }

    def run(args: List[String]): IO[ExitCode] =
      (buildConsumer concurrently buildServer)
        .compile
        .drain
        .as(ExitCode.Success)

    private[this] def buildServer = for {
      producer <- producerStream[IO, String, Name](producerSettings)
      route = routes(producer)
      server <- BlazeServerBuilder[IO].bindHttp(8080, "0.0.0.0").withHttpApp(route.orNotFound).serve
    } yield server

    private def buildConsumer =
      Stream.bracket(IO { logger.info("Consumer start") })(_ => IO { logger.info("Consumer stop") })
        .flatMap { _ =>
          consumerStream[IO]
            .using(consumerSettings)
            .evalTap(_.subscribeTo("topic"))
            .flatMap(_.stream)
            .map(_.record.value)
            .map(println)
        }

    private[this] implicit val cs: ContextShift[IO] = IO.contextShift(global)


    private[this] implicit val nameSerializer: Serializer[Name] =
      Serializer.lift[Name](HelloEndpoint.nameCodec.encode(_).getBytes("UTF-8"))

    private[this] implicit val nameDeserializer: Deserializer[Name] =
      Deserializer.lift[Name](bytes => Name(bytes.toString))

    private[this] def routes(producer: KafkaProducer[IO, String, Name]) = Router(
      "/swagger.yaml" -> swagger,
      "/v1" -> HelloEndpoint(producer),
      "/docs" -> webjars)

    private[this] val webjars: HttpRoutes[IO] = webjarService(Config(global))

    private[this] val swagger = HttpRoutes.of[IO] {
      case GET -> Root => Ok(HelloEndpoint.definition.toOpenAPI("My endpoint", "1.0").toYaml)
    }

    private[this] val producerSettings = ProducerSettings[String, Name].withBootstrapServers(bootstrapServers)

  }.main(args)
}

