package name.bastkowski.httpkafka

case class KafkaConfig(bootstrapServers: String)
